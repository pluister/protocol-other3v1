# Protocol group other3
This is version `other3V1.2` of the protocol for the programming project of module 05 of TCS.
Changes from older versions can be found in the CHANGELOG.
Any remarks or questions can be asked to Daan Pluister on the Software Systems slack.

## Protocol format
- Every command or response is indicated in a `code block`. 
- `<argument>` required argument.
- `[argument]` optional input or argument.
- `[argument]*` optional input or argument 0 or multiple times.
- `<option1|option2>` required argument could be option1 or option2.

## General message format
- Delimiter is a semi colon. You do not enter a semi colon after the last argument or command otherwise this will be handled as a new argument.
- There should not be spaces right before or after a semi colon. These will be treated as part of the argument or command.
- Every command is a single lower case char. There is always at least a command and zero or more arguments may be added.
- If there are two `code` `blocks` it means that the server sends multiple messages. Between these two code blocks should be a line seperator `\n`. The last of the multiple lines should be the end text message: `---EOT---`.

### Example
`<command>;<argument>;<argument>;[optional argument]`

## Client -> Server

### Connect
The client says hello to the server, sends what protocol they will use, and send which extensions they have enabled (if any).

#### Syntax
`H;<protocol version>[;extension name]*`

#### Examples
`h;other3V1.2`
`h;other3V1.2;chat`
`h;other3V1.2;chat;security`
`h;other3V1.2;security;chat`

### Lobby request
When a client sends a lobby request it is expected that the server returns with lobbies.
#### Syntax
`l`

### Join lobby
The client can join an existing or new lobby with the join command and indicate theirname and team name.

#### Syntax
`j;<lobby name>;<player name>;<team name>`
#### Examples
`j;4w3s0m3sw4g lobby;John;team Cool` OR 
`j;NewLobby;Alice;myTeam`

### Game ready
When a client is ready to start the game a ready request is send. When a client sends a game ready request it is expected that the server notifies the other clients.

#### Sytnax
`r`

### Game move
If the turn indicates your color, you indicate your move. The client indicates which marble is initiating the move (marble 1), which marble is the last in the row of marbles they want to move (marble 2), and the destination of marble 1. If only one marble is moved, the first and second argument should be the same. For the coordinate system we use the Wikipedia notation: https://en.wikipedia.org/wiki/Abalone_(board_game)#Move_notation (Capital letter A-I followed by a number 1-9)
#### Syntax
`m;<coordinates of tail marble>;<coordinates of head marble>;<destination of tail marble>`
#### Examples
If you want to move a2, b3 and c4 to b3, c4 and d5, the following should be send to the server: `m;A2;C4;B3`

### Disconnect from lobby/game (intentional)
If the client wants to disconnect from the lobby or while a game is not running, they send an exit command. After sending an exit the client stays connected to the server but has to join a new lobby to play a game. A client can always exit.

#### Syntax
`x`

### Close connection (intentional)
A client can only close the connection when he is not in a lobby or game. The client sends an exit command.
#### Syntax
`x`

## Server -> Client
### Confirm connect
After receiving a hello request from a client that uses the same version of the protocol, the server responds with hello and then lists all available lobbies as it would do described in "Provide lobbies".
#### Syntax
`h`
for each lobby: `l;<lobby name>;p;<player name>;<team name>[;p;<player name>;<team name>]*`
`---EOT---`
#### Example
`h`
`l;Jelles Lobby;p;Jelle;team Cool`
`l;4w3s0m3sw4g lobby;p:Alice;team Cool;black;p;Bob;team Hot;blue;p;Chris;team Cool;white;p;Daan P;team Hot;red`
`---EOT---` OR

`h`
`---EOT---` (if no lobbies available)

### Provide lobbies
After receiving a lobby request the server lists all available lobbies together with the players in that lobby.

#### Syntax
for each lobby: `l;<lobby name>;p;<player name>;<team name>[;p;<player name>;<team name>]*`
`---EOT---`
#### Example
`l;Jelles Lobby;p;Jelle;team Cool`
`l;4w3s0m3sw4g lobby;p:Alice;team Cool;black;p;Bob;team Hot;blue;p;Chris;team Cool;white;p;Daan P;team Hot;red`
`---EOT---` OR 

`---EOT---`
(if no lobbies available)

### Join game
After receiving a join request the server can respond in a number of ways. 

#### successfull join
If the join request or lobby create request is successful, it will confirm the join by sending the join to all clients in that lobby. The ready status of all players in the lobby will be removed, no message is send about this. Additionally the server send a new message with the current status of the lobby to the new player.

##### Syntax
To all clients in the lobby:
`j;<player name>;<team name>`

Addtionally to the client who connected:
`l;<lobby name>;p;<player name>;<team name>[;p;<player name>;<team name>]*`

##### Examples
To all clients in the lobby
`j;John;team Cool` OR 
`j;Alice;myTeam`

Addtionally to the client who connected:
`l;Jelles Lobby;p;Jelle;team Cool;p;John;team Cool` OR
`l;Jelles Lobby;p;Jelle;team Cool;p;Alice;myTeam`

#### unsuccessfull join 
If the server found the joined lobby is full (there are already 4 players in the lobby) or there is another player in the lobby with the same player name AND team name, the server will send an exception type 3 and tell the client what is wrong. Available messages are (`player with that name and team exists in lobby` or `lobby full`).

##### Syntax
`e;3;<Message>`

##### Examples
`e;3;player with that name and team exists in lobby` OR
`e;3;lobby full`

### Confirm ready
When a ready request is send the server sends a conformation to all clients in the lobby. If the last player sends a ready request the server will send a start specified at "Game Start".

#### Syntax
`r;<player name>`

#### Example
`r;Daan P;Hot team`

### Game start
When the last client sends ready the server will send that the game is ready to all clients or sends the error to all clients.

#### accepted Game start
When the last client sends ready and the game can start the server will send a start game and lists which player in comination with teamname gets which color (the team name is included because players on different teams can have the same name). 

#### Syntax
`s;<name white player>;<team name that player>;<name black player>;<team name that player>;[;<name blue player>;<team name that player>;][;<name red player>;<team name that player>;]`

#### Examples
`s;John;team Cool;John;Hot team` OR

`s;Alice;team Cool;Bob;Hot team;Chris;neither Cool or Hot` OR

`s;Alice;team Cool;Bob;team Hot;Chris;team Cool;Daan P;team Hot`

#### unaccepted Game start 
If the game can not start the server will send an exception type 3 to all clients. And all clients ready status is removed. The message can be: `teams do not match` (if there are four players where there are not two teams with both two players) or `not enough players` (if there is only one player).

##### Syntax
`e;3;<Message>`
##### Example
`e;3;only one player in lobby` OR 
`e;3;teams do not match`

### Turn
Every turn, the server indicates to all clients who can make a move giving the color.
#### Syntax
`t;<black|white|blue|red>`
#### Example
`t;black`

### Move
After a move request the server checks the validity of the move and then sends message described in "Accepted move" and "Inaccepted move" accordingly..

#### Accepted move
If the move is accepted, the server confirms the move by forwarding the move to all clients. The new turn can be send after this.
##### Syntax
`m;<coordinates of marble 1>;<coordinates of marble 2>;<destinationof marble 1>`

#### Unaccepted move
If the move is deemed illegal by the server, the server responds only to the client whose turn it is. The client can then try another move.
##### Syntax
`u`

#### Errors
If wrong clients requests a move an error of type 3 is send with the following message: `not your turn`.
##### Syntax
`e;3;not your turn`

### Game end
When a game ends the server sends the server sends the result of the game to all clients. (Possible results can be: `game won`, `draw`, `disconnection`). If the game is won, the server also sends the colors of the one or two winner(s). If the game has ended in a draw, no colors are given. In any case new lobby will be created with the players that are still connected.

#### Syntax
`g;<result>[;color[;color]]`

#### Examples
`g;game won;black;white` OR
`g;game won;black` OR
`g;draw`

### Client disconnect from lobby/game (intentional)
If an exit message is received from a client in lobby state the server notifies the other clients in the lobby by sending an exit command together with the player name. The ready status is of every client in the lobby is removed. No message need to be send for this. <br>
If the player is in game a game command is send that the game has ended. The game has ended with a disconnection, the server also sends a game command togehter with the color of the disconnected client. In a 2 to 3 player game the other colors won, in a 4 player game the opposite team won.

#### Syntax
`x;<player name>;<team name>` OR 
`g;disconnection;<color>`

#### Example
`x;Daan P;Hot team` (in lobby status) OR
`g;disconnection;black` (in game status)

### Close connection (intentional)
A client can only close the connection when he is not in a lobby or game. The client sends an exit command.
#### Syntax
`x`


## Errors
The server can send the client an error if the server receives something unexpected. The client is not able to send an error message.

### Unexpected command
If the first char is not any of the given commands that can be send by the client the server sends an error of type 1.

#### Syntax
`e;1`

### Unexcpected argument
If the server finds out that the number of arguments is not possible with the given command the server sends an error of type 2.

#### Syntax
`e;2`

## Commands
### Commands that can be send by the client
| char | message | description                                                |
|------|---------|------------------------------------------------------------|
| h    | HELLO   | Indicate a new connection to the server                    |
| l    | LOBBY   | Request all lobbies                                        |
| j    | JOIN    | Indicate a new connection to a lobby or create a new lobby |
| r    | READY   | Indicate you are ready to start a game                     |
| m    | MOVE    | Make a move                                                |
| x    | EXIT    | Exit the current lobby or game or close connection         |

### List of server responses
| char | message | description                                                |
|------|---------|------------------------------------------------------------|
| h    | HELLO   | Recognise connection to the server                         |
| l    | LOBBY   | List all lobbies                                           |
| j    | JOIN    | Indicate a client has joined a lobby or created a new lobby|
| r    | READY   | Indicate that a client is ready to start the game          |
| s    | START   | Indicate the game has started                              |
| t    | TURN    | Undicate whose turn it is                                  |
| m    | MOVE    | Approve a move and distribute it to other clients          |
| g    | GAME_OVER   | Indicate the current game is finished                  |

### Improper messages
There are a number of improper messages a server can receive. They are split into 4 categories.

| char        | message             | description                                                                                            |
|-------------|---------------------|--------------------------------------------------------------------------------------------------------|
| e;1         | Unexpected command  | The command letter is not available. |
| e;2         | Unexpected argument | One or more arguments is invalid. |
| e;3         | Illegal argument    | Performing this action is expected but not allowed. The server will indicate what is wrong. Examples: not your turn, lobby is full, etc. |
| u           | Unaccepted move     | Special case of e;3, the performed move is not allowed by the server. The client can try again.   |

## Notes
- A client can join a 3 player lobby even if the teams do not match. If that happens a game can never start. To make this user friendly one can implement a change team name function using the exit and join client commands.
- The number of clients in a lobby and the amount of lobbies is not fixed. This means anyone can join any lobby that have zero to three players.
- When at the end of a game a new lobby is created the server does not send this to the game clients, since they should still know witch players are connected.
- Only the last version of the protocol should be supported.
- If multiple messages are send the server always ends with sending the message `---EOT---`. This is choosen because in the HotelProtocol of week7 there is a readMultipleLines() method which can handle this as well.
- The server should respond to the commands given by the client no matter the state of the client (in lobby, in game, etc..). However you can implement a client which limmits the command options given the client state.
- From version `otherV1.2`, the readMultipleLines() from HotelClient can now be used. If you used the sendMessage() method from HotelClient, there is a out.newLine() command. This adds the line seperator `\n` to the end of the message. So if this is used in previous versions nothing is changed in message sending.

## Extensions
No protocol for extensions is made, if a project team wants a new extension a project team member should mention this on slack that they will be the protocol maintainer for this new extension.
