Current supported version: `Other3V1.2`

## version `Other3V1.3` (25-01-2020):
Changes in message sending:
- a general error can also have an optional error message.
- fixed the syntax of confirm ready in Server -> Client

Changes in descriptions:
- Changed `H` to `h` in the syntax of client -> server send hello
- Removed the colors in the example of server -> client lobby

## version `Other3V1.2` (22-01-2020):
Changes in message sending:
- Added the player team name at diconnect from lobby/game in Server -> Client.
- Added the player team name at ready in Server -> Client.
- For join in Server -> Client added that the current lobby status is send to the player connecting.
- Changed that if a server sends multiple lines that `\n` is added in between lines.

Changes in descriptions:
- Specified that the confirm ready in Server -> Client is send to all clients in the lobby.
- Changed file name of current protocol to `protocol_other3VCurrent`.
- Updated the command tables to include the ready command.
- Added explanation on why `\n` is added in the notes that if you used sendMessage() from HotelClient it already implemented the newline character `\n`.

## version `Other3V1.1` (20-01-2020):
- Specified that when a client joins a lobby the ready status is removed from all other clients in the lobby.
- Specified that when a client exits a lobby the ready status is removed from all other clients in the lobby.
- Specified the turn message is send to all clients.

## version `Other3V1` (17-01-2020):
Added changes based on the protocol session January 17th:
- Added Notes section that awnsers questions from the session which do not influence the protocol, but can help you on how to implement the protocol.
- Moved assign colors from join lobby to game start.
- Added game ready command, a game will start if all players are ready.
- Specified lobby max is 4 clients.
- Added when a client leaves a lobby other clients will be notified.

Other changes:
-Added Error section

# beta versions `Other3V0`:

## version `Other3V0.95` (15-01-2020):
- Specified the move message. No changes in sending and receiving messages

## version `Other3V0.9` (10-01-2020):
- Created protocol based on Jelle's notes
